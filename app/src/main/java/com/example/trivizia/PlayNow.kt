package com.arhamsoft.online.trivizia

import android.animation.Animator
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.text.HtmlCompat
import androidx.databinding.DataBindingUtil
import com.arhamsoft.online.trivizia.DataBase.UserDatabase
import com.arhamsoft.online.trivizia.DataBase.userDataTable
import com.arhamsoft.online.trivizia.Play.Companion.postList
import com.arhamsoft.online.trivizia.databinding.ActivityPlayNowBinding
import com.google.gson.Gson


class PlayNow : AppCompatActivity() {
    var counter=0
    var currentPoints=0
    var typePoints=0
    companion object{
        var totalAttemptsQuestion=0
    }
    var Lines:List<String>?=null
    var correctOption:String?=null
    var databaseObj: UserDatabase?=null
    var arr=ArrayList<String>()
    lateinit var sharedPreferences: SharedPreferences
    lateinit var binding: ActivityPlayNowBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this, R.layout.activity_play_now)
       databaseObj = UserDatabase.getDatabase(this)
        sharedPreferences=getSharedPreferences("attpName", MODE_PRIVATE)

        Lines = resources.getStringArray(R.array.category).asList()

        binding.countDownLayout.visibility = View.VISIBLE
        binding.animationViewLoading.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(p0: Animator?) {

                binding.wholePlayNowLayout.visibility = View.GONE

            }

            override fun onAnimationEnd(p0: Animator?) {
                    //firstTime call
                binding.countDownLayout.visibility = View.GONE
                binding.wholePlayNowLayout.visibility = View.VISIBLE
                begin_quiz()

            }

            override fun onAnimationCancel(p0: Animator?) {
                TODO("Not yet implemented")
            }

            override fun onAnimationRepeat(p0: Animator?) {
                TODO("Not yet implemented")
            }

        })



        val th=Thread(Runnable {
            var a =UserDatabase.getDatabase(this).datauserDao().getpoints()
            if(a!=null)
                binding.bottomPoints.text=a.toString()+" Points"
            else
                binding.bottomPoints.text="0 Points"
        })
        th.start()
        th.join()
        Log.d("mcqs1",correctOption.toString())

        binding.leavegame.setOnClickListener {
            alerdialog()
        }

    }

    fun alerdialog()
    {
        var alertDialog= AlertDialog.Builder(this)
        alertDialog
            .setMessage("Are you sure you want to leave?")
            .setCancelable(false)
            .setNegativeButton("No", DialogInterface.OnClickListener { dialogInterface, which ->
                dialogInterface.cancel()

            }).setPositiveButton("Yes", DialogInterface.OnClickListener{ dialogInterface, which ->
                dialogInterface
                onBackPressed()

            })
        alertDialog.create().show()
    }
    fun alerdialog2()
    {
        // points tell dialog
        var alertDialog= AlertDialog.Builder(this)
        alertDialog
            .setMessage("You earn "+currentPoints+" points")
            .setCancelable(false)
            .setPositiveButton("OK", DialogInterface.OnClickListener{ dialogInterface, which ->
                dialogInterface
                onBackPressed()

            })
        alertDialog.create().show()
    }



    fun onMCQ1(view: View)
    {
        //for mcq option 1
        Log.d("mcqs",correctOption.toString())
        if(!binding.a.text.equals(correctOption))
        {
            Insert(0,binding.a.text.toString(),0)
        }
        else {
            calculatePoints()
            Insert(typePoints,binding.a.text.toString(),1)
        }
        tikIconAnaimation(binding.a.text.equals(correctOption))

    }
    fun onMCQ2(view: View)
    {
        //for mcq option 2
        Log.d("mcqs",correctOption.toString())
        if(!binding.b.text.equals(correctOption))
        {

            Insert(0,binding.b.text.toString(),0)
        }
        else {
            calculatePoints()
            Insert(typePoints,binding.b.text.toString(),1)
        }
        tikIconAnaimation(binding.b.text.equals(correctOption))

    }
    fun onMCQ3(view: View)
    {
        //mcq option 3
        Log.d("mcqs",correctOption.toString())
        if(!binding.c.text.equals(correctOption))
        {

            Insert(0,binding.c.text.toString(),0)
        }
        else {
            calculatePoints()
            Insert(typePoints,binding.c.text.toString(),1)
        }
        tikIconAnaimation(binding.c.text.equals(correctOption))

    }
    fun onMCQ4(view: View)
    {
        //mcq option 4
        Log.d("mcqs",correctOption.toString())
        if(!binding.d.text.equals(correctOption))
        {

            Insert(0,binding.d.text.toString(),0)
        }
        else {
            calculatePoints()
            Insert(typePoints,binding.d.text.toString(),1)
        }

        tikIconAnaimation(binding.d.text.equals(correctOption))

    }
    fun onTrue(view: View)
    {
        //truefalse option true
        if(!binding.t.text.equals(correctOption))
        {

            Insert(0,binding.t.text.toString(),0)
        }
        else {
            calculatePoints()
            Insert(typePoints,binding.t.text.toString(),1)

        }
        tikIconAnaimation(binding.t.text.equals(correctOption))


    }
    fun onFalse(view: View)
    {
        //truefalse option false
        if(!binding.f.text.equals(correctOption))
        {

            Insert(0,binding.f.text.toString(),0)
        }
        else {
            calculatePoints()
            Insert(typePoints,binding.f.text.toString(),1)
        }
        tikIconAnaimation(binding.f.text.equals(correctOption))
        //onCallData()
    }


    fun begin_quiz(){
        //this function call only first time for data set

            totalAttemptsQuestion= totalAttemptsQuestion+10
            val editor=sharedPreferences.edit()
            editor.putInt("attp",(Play.attempts+1))
            editor.apply()

            Log.d("api Check",""+ Gson().toJson(postList!!.results!![counter]))
            binding.outofText.text=(counter+1).toString()+"/10"
        //html function for decode the values
            val fromApi = postList!!.results!![counter].question
            val textFromHtmlFromApi = fromApi?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
            binding.multiLineQuestion.text=textFromHtmlFromApi
            correctOption=postList!!.results!![counter].correct_answer?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
                .toString()


            if(postList!!.results!![counter].type=="boolean")
            {
                //this condition for type selection i.e true false
                binding.optionLineartruefalse.visibility = View.VISIBLE
                arr.add(postList!!.results!![counter].incorrect_answer!![0]?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
                    .toString())
                arr.add(postList!!.results!![counter].correct_answer.toString()?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
                    .toString())

                //add option in another array and shuffle the options then set in text view
                arr.shuffle()
                binding.t.text=arr[0]
                binding.f.text=arr[1]

            }
            else
            {
                //this condition for MCQs
                binding.optionLinearMCQ.visibility=View.VISIBLE
                arr.add(postList!!.results!![counter].incorrect_answer!![0]?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
                    .toString())
                arr.add(postList!!.results!![counter].incorrect_answer!![1]?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
                    .toString())
                arr.add(postList!!.results!![counter].incorrect_answer!![2]?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
                    .toString())
                arr.add(postList!!.results!![counter].correct_answer.toString()?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
                    .toString())

                arr.shuffle()
                binding.a.text=arr[0]
                binding.b.text=arr[1]
                binding.c.text=arr[2]
                binding.d.text=arr[3]

            }



    }

    fun tikIconAnaimation(isValid:Boolean){
        //this function for animation correct option and incorrect option
        val slideAnimation = AnimationUtils.loadAnimation(this, R.anim.bounceb)
        slideAnimation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {
                disableAllButtons()

            }

            override fun onAnimationRepeat(p0: Animation?) {

            }

            override fun onAnimationEnd(p0: Animation?) {

                binding.tikicon.visibility=View.GONE
                binding.crossicon.visibility=View.GONE
                onCallData()
                enableAllButtons()
               // enableAllButtons()
            }
        })
        if(isValid)
        {
            binding.tikicon.visibility=View.VISIBLE
            binding.tikicon.startAnimation(slideAnimation)

        }else{

            binding.crossicon.visibility=View.VISIBLE
            binding.crossicon.startAnimation(slideAnimation)

        }


    }




    fun disableAllButtons(){
        binding.t.isEnabled=false
        binding.f.isEnabled=false
        binding.a.isEnabled=false
        binding.b.isEnabled=false
        binding.c.isEnabled=false
        binding.d.isEnabled=false
    }
    fun enableAllButtons() {
        binding.t.isEnabled=true
        binding.f.isEnabled=true
        binding.a.isEnabled=true
        binding.b.isEnabled=true
        binding.c.isEnabled=true
        binding.d.isEnabled=true
    }



    fun Insert(tpoints:Int,your:String,staus:Int)
    {
        //this function for insert the data in database
        val th=Thread(Runnable {
            databaseObj!!.datauserDao().InsertData(
                userDataTable(
                    Lines?.get(Play.cat2)?.toString(), postList!!.results!![counter].type,
                    postList!!.results!![counter].difficulty, postList!!.results!![counter].question?.let {
                        HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }.toString(),
                    postList!!.results!![counter].correct_answer,your,tpoints,staus)
            )
        })
        th.start()
        th.join()
    }

    fun calculatePoints()
    {

        //calculate the points
        if(postList!!.results!![counter].difficulty=="hard")
        {
            currentPoints=currentPoints+3
            typePoints=3
        }
        else if(postList!!.results!![counter].difficulty=="medium")
        {
            currentPoints=currentPoints+2
            typePoints=2
        }
        else if(postList!!.results!![counter].difficulty=="easy")
        {
            currentPoints=currentPoints+1
            typePoints=1
        }
    }
    fun onCallData(){

        //this function for set the data from 2nd to 10 time.
        counter++
        if(counter<=9)
        {
            binding.outofText.text=(counter+1).toString()+"/10"
            var arr=ArrayList<String>()
            val fromApi = postList!!.results!![counter].question
            val textFromHtmlFromApi = fromApi?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
            binding.multiLineQuestion.text=textFromHtmlFromApi
            correctOption= (postList!!.results!![counter].correct_answer)?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
                .toString()
            Log.d("mcqs1",correctOption.toString())

            if(postList!!.results!![counter].type=="boolean")
            {

                binding.optionLinearMCQ.visibility=View.GONE
                binding.optionLineartruefalse.visibility = View.VISIBLE
                arr.add(postList!!.results!![counter].incorrect_answer!![0]?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
                    .toString())
                arr.add(postList!!.results!![counter].correct_answer.toString()?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
                    .toString())

                arr.shuffle()
                binding.t.text=arr[0]
                binding.f.text=arr[1]


            }
            else
            {
                binding.optionLineartruefalse.visibility=View.GONE
                binding.optionLinearMCQ.visibility=View.VISIBLE


                arr.add(postList!!.results!![counter].incorrect_answer!![0]?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
                    .toString())
                arr.add(postList!!.results!![counter].incorrect_answer!![1]?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
                    .toString())
                arr.add(postList!!.results!![counter].incorrect_answer!![2]?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
                    .toString())
                arr.add(postList!!.results!![counter].correct_answer.toString()?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }
                    .toString())

                arr.shuffle()
                binding.a.text=arr[0]
                binding.b.text=arr[1]
                binding.c.text=arr[2]
                binding.d.text=arr[3]

            }

        }
        else
        {
            alerdialog2()
        }

    }

    override fun onBackPressed() {
        //for restric back when loading
        if(!binding.animationViewLoading.isAnimating)
        {
            super.onBackPressed()
        }
        else
        {
            Toast.makeText(this, "Please wait", Toast.LENGTH_SHORT).show()
        }


    }

}

