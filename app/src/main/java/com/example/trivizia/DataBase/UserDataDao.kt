package com.arhamsoft.online.trivizia.DataBase

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface UserDataDao{


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun InsertData(userdata:userDataTable)

    @Query("SELECT * FROM UserDataTable")
    fun getData():List<userDataTable>

    @Query("SELECT SUM(points) FROM UserDataTable")
    fun getpoints():Int?

    @Query("SELECT Count(status) FROM UserDataTable where status=0")
    fun getWrongs():Int?

    @Query("SELECT Count(status) FROM UserDataTable where status=1")
    fun getRights():Int?

    @Query("SELECT Count(id) FROM UserDataTable")
    fun getTotalQuestion():Int?

    @Query("SELECT SUM(points) FROM UserDataTable where cat=:str")
    fun getCatPoints(str:String):Int
    @Query("SELECT SUM(points) FROM UserDataTable")
    fun getAnyCatPoints():Int



}