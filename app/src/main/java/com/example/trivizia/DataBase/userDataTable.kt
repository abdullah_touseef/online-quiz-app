package com.arhamsoft.online.trivizia.DataBase

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName="UserDataTable")
data class userDataTable(

    var cat:String?=null,
    var type:String?=null,
    var diff:String?=null,
    var question:String?=null,
    var correctAnswer:String?=null,
    var yourAnswer:String?=null,
    var points:Int=0,
    var status:Int=2

)
{
    @PrimaryKey(autoGenerate = true)
    var id:Int=0
}

