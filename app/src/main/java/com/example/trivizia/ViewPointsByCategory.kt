package com.arhamsoft.online.trivizia

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.arhamsoft.online.trivizia.Adapters.MyAdapterCategory
import com.arhamsoft.online.trivizia.databinding.ActivityViewPointsByCategoryBinding


class ViewPointsByCategory : AppCompatActivity() {
    lateinit var binding: ActivityViewPointsByCategoryBinding
    var catList:ArrayList<Int>? =ArrayList()
    var pt:Int=0
    var Lines:List<String>?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_view_points_by_category)

        Lines = resources.getStringArray(R.array.category).asList()

        binding.backArrow.setOnClickListener {
           onBackPressed()
        }
           binding.PointsCategoryRecyler.layoutManager= LinearLayoutManager(this)
           binding.PointsCategoryRecyler.adapter= MyAdapterCategory(Lines!!,pt,this)

    }
    override fun onResume() {
        super.onResume()
    }

}