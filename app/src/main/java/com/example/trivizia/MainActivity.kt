package com.arhamsoft.online.trivizia

import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.arhamsoft.online.trivizia.DataBase.UserDatabase
import com.arhamsoft.online.trivizia.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    lateinit var sharedPreferences:SharedPreferences
    lateinit var sharedPreferences2: SharedPreferences
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_main)
        sharedPreferences=getSharedPreferences("ProfileName", MODE_PRIVATE)
        sharedPreferences2=getSharedPreferences("attpName", MODE_PRIVATE)

        binding.youId.text=sharedPreferences.getString("name","You")


        val th=Thread(Runnable {
            var a = UserDatabase.getDatabase(this).datauserDao().getpoints()
            if(a!=null)
                binding.pointsId.text=a.toString()+" Points"
            else
                binding.pointsId.text="0 Points"


        })
        th.start()
        th.join()



        Play.attempts=(sharedPreferences2.getInt("attp",0))
        binding.attempId.text="Attempts " + "("+(sharedPreferences2.getInt("attp",0))+ ")"

        binding.playButton.setOnClickListener {
            startActivity(Intent(this,Play::class.java))


        }

        binding.staticButton.setOnClickListener {
            startActivity(Intent(this,Statistics::class.java))


        }

        binding.profileButton.setOnClickListener {
            startActivity(Intent(this,Profile::class.java))


        }

        binding.viewAttempQuestion.setOnClickListener {
            startActivity(Intent(this,ViewAttemptQuestions::class.java))


        }

        binding.viewPoints.setOnClickListener {
            startActivity(Intent(this,ViewPointsByCategory::class.java))


        }
        binding.settingButton.setOnClickListener {
            alerdialog2()
        }
        binding.quickPlayButton.setOnClickListener {
            Toast.makeText(this, "Not Available This Mode", Toast.LENGTH_SHORT).show()
        }
    }

    fun alerdialog2()
    {
        var alertDialog= AlertDialog.Builder(this)
        alertDialog
            .setMessage("no setting available")
            .setCancelable(false)
            .setPositiveButton("OK", DialogInterface.OnClickListener{ dialogInterface, which ->
                dialogInterface


            })
        alertDialog.create().show()
    }

    override fun onResume() {
        super.onResume()
        binding.youId.text=sharedPreferences.getString("name","You")
        Play.attempts=(sharedPreferences2.getInt("attp",0))
        binding.attempId.text="Attempts " + "("+(sharedPreferences2.getInt("attp",0))+ ")"
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}