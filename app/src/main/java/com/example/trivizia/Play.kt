package com.arhamsoft.online.trivizia

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.arhamsoft.online.trivizia.RetRofitAPI.QuizData
import com.arhamsoft.online.trivizia.RetRofitAPI.RetroInterface
import com.arhamsoft.online.trivizia.databinding.ActivityPlayBinding


import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Play : AppCompatActivity() {
    lateinit var binding: ActivityPlayBinding

    val gson = Gson()

    var generatedURL:String?="api.php?amount=10"
    companion object{
        var postList: QuizData?=null
        var cat=0
        var cat2=0
        var diff:String?=null
        var type:String?=null
        var attempts=0


    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_play)

        binding.backArrow.setOnClickListener {
            onBackPressed()
        }

        //category
        var arrayAdapter=ArrayAdapter.createFromResource(
            this,
            R.array.category,
            android.R.layout.simple_spinner_item
        )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.anyCategory.adapter=arrayAdapter
        binding.anyCategory.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position==0)
                {
                    cat=0
                    cat2=0
                }
                else
                {
                    cat=position+8
                    cat2=position
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        })
        ///difficulty
        var arrayAdapter2=ArrayAdapter.createFromResource(
            this,
            R.array.Difficulty,
            android.R.layout.simple_spinner_item
        )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.anyDifficulty.adapter=arrayAdapter2
        binding.anyDifficulty.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

               //var c2=0
                if(position==0)
                {
                    diff=null
                }
                else if(position==1)
                {
                    diff="easy"
                }
                else if(position==2)
                {
                    diff="medium"
                }
                else if(position==3)
                {
                   diff="hard"
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        })


        ///types
        var arrayAdapter3=ArrayAdapter.createFromResource(
            this,
            R.array.TYPE,
            android.R.layout.simple_spinner_item
        )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.anyType.adapter=arrayAdapter3
        binding.anyType.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position==0)
                {
                    type=null
                }
                else if(position==1)
                {
                   type="multiple"
                }
                else if(position==2)
                {
                   type="boolean"
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        })

        binding.playNow.setOnClickListener {


            if(isOnline(this))
            {
                binding.playNow.isEnabled=false

                //Creating instance of retro API and setting URL
                val rf= Retrofit.Builder()
                    .baseUrl(RetroInterface.base_url)
                    .addConverterFactory(GsonConverterFactory.create()).build()

                binding.animationViewloading.visibility=View.VISIBLE

                val API=rf.create(RetroInterface::class.java)
                if(Play.cat!=0)
                {
                    generatedURL+="&category="+Play.cat
                }
                if(Play.diff!=null)
                {
                    generatedURL+="&difficulty="+Play.diff
                }
                if(Play.type!=null)
                {
                    generatedURL+="&type="+Play.type
                }

                Log.e("URL",generatedURL.toString())

                val call= generatedURL?.let { API.posts(it) }
                call?.enqueue(object : Callback<QuizData?> {

                    //this function for API response and also failure response
                    override fun onResponse(
                        call: Call<QuizData?>,
                        response: Response<QuizData?>
                    ) {
                        postList =response.body()
                        if(postList!!.results!!.isNotEmpty())
                        {
                            binding.animationViewloading.visibility=View.GONE
                            loading()
                            binding.playNow.isEnabled=true
                        }
                        else
                        {binding.animationViewloading.visibility=View.GONE

                            alerdialog3()
                            binding.playNow.isEnabled=true
                        }

                        // Log.d("API2 Check",""+ Gson().toJson(postList!!.results!!))
                    }
                    override fun onFailure(call: Call<QuizData?>, t: Throwable) {
                        binding.animationViewloading.visibility=View.GONE
                        Toast.makeText(applicationContext, "Something went Wrong", Toast.LENGTH_SHORT).show()
                        binding.playNow.isEnabled=true
                    }

                })

            }
            else
            {
                alerdialog4()
            }

        }

    }

    fun loading(){
            startActivity(Intent(this,PlayNow::class.java))
    }

    fun alertdialog2()
    {
        var alertDialog= AlertDialog.Builder(applicationContext)
        alertDialog
            .setMessage("someThing Gone Wrong")
            .setCancelable(false)
            .setPositiveButton("OK", DialogInterface.OnClickListener{ dialogInterface, which ->
                dialogInterface


            })
        alertDialog.create().show()
    }


    fun alerdialog3()
    {
        var alertDialog= AlertDialog.Builder(this)
        alertDialog
            .setMessage("No Question Found In your Match...!")
            .setCancelable(false)
            .setPositiveButton("OK", DialogInterface.OnClickListener{ dialogInterface, which ->
                dialogInterface


            })
        alertDialog.create().show()
    }
    fun alerdialog4()
    {
        var alertDialog= AlertDialog.Builder(this)
        alertDialog
            .setMessage("No internet connection")
            .setCancelable(false)
            .setPositiveButton("OK", DialogInterface.OnClickListener{ dialogInterface, which ->
                dialogInterface


            })
        alertDialog.create().show()
    }

    fun isOnline(context: Context): Boolean {

        //this function for internet connectivity
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val n = cm.activeNetwork
            if (n != null) {
                val nc = cm.getNetworkCapabilities(n)
                //It will check for both wifi and cellular network
                return nc!!.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || nc.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
            }
            return false
        } else {
            val netInfo = cm.activeNetworkInfo
            return netInfo != null && netInfo.isConnectedOrConnecting
        }
    }
    override fun onBackPressed() {
        //for restric back when loading
        if(!binding.animationViewloading2.isAnimating)
        {
            super.onBackPressed()
        }
        else
        {
            Toast.makeText(this, "Please wait", Toast.LENGTH_SHORT).show()
        }


    }
}

