package com.arhamsoft.online.trivizia

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import com.arhamsoft.online.trivizia.databinding.ActivitySplashBinding


class Splash : AppCompatActivity() {
    lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_splash)

        val slideAnimation = AnimationUtils.loadAnimation(this, R.anim.bounceb)
        binding.splashlogo.startAnimation(slideAnimation)

        Handler().postDelayed(Runnable
        {
            val i = Intent(this,MainActivity::class.java)
            startActivity(i)
            finish()

        }, 2 * 1000
        )
    }

    override fun onBackPressed() {

    }
}