package com.arhamsoft.online.trivizia

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.arhamsoft.online.trivizia.DataBase.UserDatabase
import com.arhamsoft.online.trivizia.databinding.ActivityStatisticsBinding

class Statistics : AppCompatActivity() {



    lateinit var binding: ActivityStatisticsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_statistics)


        binding.totalattemptsNo.text=(Play.attempts.toString())
        binding.totalquestionsNo.text=PlayNow.totalAttemptsQuestion.toString()

        val th=Thread(Runnable {
            // set the statistics of user
            var a =UserDatabase.getDatabase(this).datauserDao().getpoints()
            if(a!=null)
                binding.PointsNo.text=a.toString()
            else
                binding.PointsNo.text="0"

            binding.IncorrectAnswersNo.text= UserDatabase?.getDatabase(this)?.datauserDao()?.getWrongs().toString()
            binding.correctAnswersNo.text= UserDatabase?.getDatabase(this)?.datauserDao()?.getRights().toString()
            binding.totalattemptsNo.text= UserDatabase?.getDatabase(this).datauserDao()?.getTotalQuestion().toString()


        })
        th.start()
        th.join()


        binding.backArrow.setOnClickListener {
            onBackPressed()
        }
    }


}