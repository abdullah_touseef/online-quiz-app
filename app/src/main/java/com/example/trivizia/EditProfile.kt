package com.arhamsoft.online.trivizia

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.arhamsoft.online.trivizia.databinding.ActivityEditProfileBinding


class EditProfile : AppCompatActivity() {
    lateinit var sharedPreferences:SharedPreferences
    lateinit var binding: ActivityEditProfileBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_edit_profile)

        sharedPreferences=getSharedPreferences("ProfileName", MODE_PRIVATE)

        binding.youIdprofile.setText(sharedPreferences.getString("name",""))

        binding.updateprofile.setOnClickListener {
            val editor=sharedPreferences.edit()

            if(binding.youIdprofile.text.isEmpty())
                editor.putString("name","YOU")
            else
                editor.putString("name",binding.youIdprofile.text.toString())
            editor.apply()
            onBackPressed()


        }
        binding.backArrow.setOnClickListener {
            onBackPressed()
        }

    }


}