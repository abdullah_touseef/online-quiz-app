package com.arhamsoft.online.trivizia

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.arhamsoft.online.trivizia.DataBase.UserDatabase
import com.arhamsoft.online.trivizia.databinding.ActivityProfileBinding


class Profile : AppCompatActivity() {
    lateinit var sharedPreferences: SharedPreferences
    lateinit var binding: ActivityProfileBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_profile)
        sharedPreferences=getSharedPreferences("ProfileName", MODE_PRIVATE)

        binding.youIdprofile.text=sharedPreferences.getString("name","You")


        binding.attempIdprofile.text="Attempts "+"("+(Play.attempts.toString())+")"

        val th=Thread(Runnable {
          // set the statistics of user
            var a = UserDatabase.getDatabase(this).datauserDao().getpoints()
            if(a!=null)
            {
                binding.pointsIdprofile.text=a.toString()+" Points"
                binding.PointsNo.text=a.toString()
            }

            else
            {
                binding.pointsIdprofile.text="0 Points"
                binding.PointsNo.text="0"
            }

            binding.IncorrectAnswersNo.text= UserDatabase?.getDatabase(this)?.datauserDao()?.getWrongs().toString()
            binding.correctAnswersNo.text= UserDatabase?.getDatabase(this)?.datauserDao()?.getRights().toString()
            binding.totalquestionsNo.text= UserDatabase?.getDatabase(this).datauserDao()?.getTotalQuestion().toString()

        })
        th.start()
        th.join()

        binding.backArrow.setOnClickListener {
           onBackPressed()

        }

        binding.editpen.setOnClickListener {

            startActivity(Intent(this,EditProfile::class.java))

        }

    }

    override fun onResume() {
        super.onResume()
        binding.youIdprofile.text=sharedPreferences.getString("name","You")
    }
}

