package com.arhamsoft.online.trivizia.RetRofitAPI



import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url


interface RetroInterface {
    @GET
    fun posts(@Url generatedURL:String): Call<QuizData?>

    companion object{
        const val base_url="https://opentdb.com/"
    }
}