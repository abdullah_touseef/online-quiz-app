package com.arhamsoft.online.trivizia.RetRofitAPI
import com.google.gson.annotations.SerializedName



data class UserData(

    @SerializedName("category")
    var category:String?=null,
    @SerializedName("type")
    var type:String?=null,
    @SerializedName("difficulty")
    var difficulty:String?=null,
    @SerializedName("question")
    var question:String?=null,
    @SerializedName("correct_answer")
    var correct_answer:String?=null,
    @SerializedName("incorrect_answers")
    var incorrect_answer:List<String>?=null,

)
