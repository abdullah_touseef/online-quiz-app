package com.arhamsoft.online.trivizia.RetRofitAPI

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class QuizData:Serializable {

    @SerializedName("results")
    var results:List<UserData>?=null
}