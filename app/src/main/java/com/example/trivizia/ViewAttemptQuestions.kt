package com.arhamsoft.online.trivizia

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.arhamsoft.online.trivizia.Adapters.MyAdapter
import com.arhamsoft.online.trivizia.DataBase.UserDatabase
import com.arhamsoft.online.trivizia.DataBase.userDataTable
import com.arhamsoft.online.trivizia.databinding.ActivityViewAttemptQuestionsBinding


class ViewAttemptQuestions : AppCompatActivity() {
    var dataList:List<userDataTable> =ArrayList()
    lateinit var binding: ActivityViewAttemptQuestionsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_view_attempt_questions)


        binding.backArrow.setOnClickListener {
            onBackPressed()
        }
        binding.attemptRecyler.layoutManager= LinearLayoutManager(this)

        val th=Thread(Runnable {
            dataList= UserDatabase?.getDatabase(this)?.datauserDao()?.getData()
        })
        th.start()
        th.join()

        binding.attemptRecyler.adapter= MyAdapter(dataList,object : MyAdapter.onItemClick{
            override fun onClick()
            {
                //this is extra no use of this function in this
            }

        },this)//3 parameters

    }

}